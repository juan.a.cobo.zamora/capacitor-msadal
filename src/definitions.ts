declare module '@capacitor/core' {
  interface PluginRegistry {
    MsADAL: MsADALPlugin;
  }
}

export interface MsADALPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  login(): Promise<boolean>;
}
