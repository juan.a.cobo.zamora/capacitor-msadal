import { WebPlugin } from '@capacitor/core';
import { MsADALPlugin } from './definitions';

export class MsADALWeb extends WebPlugin implements MsADALPlugin {
  constructor() {
    super({
      name: 'MsADAL',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async login(): Promise<boolean> {
    console.log("LOGIN")
    return true;
  }
}

const MsADAL = new MsADALWeb();

export { MsADAL };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(MsADAL);
